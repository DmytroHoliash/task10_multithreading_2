import com.holiash.view.View;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

  public static void main(String[] args) {
    run(new View());
  }

  public static void run(View view) {
    int choice = 0;
    do {
      view.printMenu();
      System.out.println("Make your choice: ");
      try {
        choice = Integer.parseInt(br.readLine());
        view.run(choice);
      } catch (IOException | NumberFormatException e) {
        System.out.println(e.getMessage());
      }
    } while (choice != 0);
  }
}
