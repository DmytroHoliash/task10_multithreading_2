package com.holiash.controller;

import com.holiash.model.task1.Task1;
import com.holiash.model.task2.Task2;
import com.holiash.model.task3.MyLockTest;

public class Controller {

  public void task1() {
    new Task1().run();
  }

  public void task2() {
    new Task2().run();
  }

  public void task3() {
    new MyLockTest().run();
  }
}
