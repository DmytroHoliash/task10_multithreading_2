package com.holiash.view;

@FunctionalInterface
public interface Command {

  void execute();
}
