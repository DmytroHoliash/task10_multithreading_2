package com.holiash.view;

import com.holiash.controller.Controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class View {
  private List<String> menu;
  private Map<Integer, Command> menuCommands;
  private Controller controller;
  private ResourceBundle resourceBundle = ResourceBundle.getBundle("menu");

  public View() {
    this.menu = new ArrayList<>();
    this.menuCommands = new HashMap<>();
    controller = new Controller();
    initMenu();
    initMenuMethods();
  }

  private void initMenu() {
    this.menu.add(resourceBundle.getString("1"));
    this.menu.add(resourceBundle.getString("2"));
    this.menu.add(resourceBundle.getString("3"));
    this.menu.add(resourceBundle.getString("4"));
  }

  private void initMenuMethods() {
    menuCommands.put(1, controller::task1);
    menuCommands.put(2, controller::task2);
    menuCommands.put(3, controller::task3);
  }

  public void printMenu() {
    this.menu.forEach(System.out::println);
  }

  public void run(int choice) {
    if (this.menuCommands.containsKey(choice)) {
      menuCommands.get(choice).execute();
    }
  }
}
