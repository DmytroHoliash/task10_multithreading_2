package com.holiash.model.task1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Task1 {

  private final Lock lock = new ReentrantLock();


  public void run() {
    Thread t1 = new Thread(this::method1);
    Thread t2 = new Thread(this::method2);
    Thread t3 = new Thread(this::method3);
    t1.start();
    t2.start();
    t3.start();
    try {
      t1.join();
      t2.join();
      t3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void method1() {
    for (int i = 0; i < 5; i++) {
      lock.lock();
      try {
        System.out.println("Method1");
      } finally {
        lock.unlock();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void method2() {
    for (int i = 0; i < 5; i++) {
      lock.lock();
      try {
        System.out.println("Method2");
      } finally {
        lock.unlock();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void method3() {
    for (int i = 0; i < 5; i++) {
      lock.lock();
      try {
        System.out.println("Method3");
      } finally {
        lock.unlock();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

}
