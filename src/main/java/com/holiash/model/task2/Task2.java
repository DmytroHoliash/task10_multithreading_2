package com.holiash.model.task2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Task2 {

  private final static int INITIAL_CAPACITY = 10;
  private static ReentrantLock lock = new ReentrantLock();
  private static Condition queueFullCondition = lock.newCondition();
  private static Condition queueEmptyCondition = lock.newCondition();

  private BlockingQueue<Integer> queue;

  public Task2() {
    queue = new ArrayBlockingQueue<>(INITIAL_CAPACITY);
  }

  public void run() {
    ExecutorService executor = Executors.newFixedThreadPool(2);

    executor.submit(() -> {
      addElement(1);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      addElement(2);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      addElement(3);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      addElement(4);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
    executor.submit(() -> {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      removeElement();
      removeElement();
      removeElement();
      removeElement();
    });
    executor.shutdown();
    try {
      if (!executor.awaitTermination(5, TimeUnit.SECONDS)) {
        executor.shutdownNow();
      }
    } catch (InterruptedException ex) {
      executor.shutdownNow();
      Thread.currentThread().interrupt();
    }
  }

  private void addElement(int element) {
    try {
      lock.lock();
      while (queue.size() == INITIAL_CAPACITY) {
        queueFullCondition.await();
      }
      queue.add(element);
      System.out.println("Add " + element + " to queue");
      queueEmptyCondition.signalAll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  private void removeElement() {
    try {
      lock.lock();
      while (queue.size() == 0) {
        queueEmptyCondition.await();
      }
      int result = queue.remove();
      System.out.println("Removed " + result + " from queue");
      queueFullCondition.signalAll();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }
}
