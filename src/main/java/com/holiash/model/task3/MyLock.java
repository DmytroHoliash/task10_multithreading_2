package com.holiash.model.task3;

public class MyLock {

  private int lockCount = 0;

  public synchronized void lock() {
    if (lockCount > 0) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    lockCount++;
  }

  public synchronized void unlock() {
    lockCount--;
    if (lockCount == 0) {
      notify();
    }
  }
}
