package com.holiash.model.task3;

import java.time.LocalDateTime;

public class MyLockTest {

  private static int A = 0;
  private final MyLock lock = new MyLock();

  public void run() {
    Thread thread1 = new Thread(this::test, "T1 ");
    Thread thread2 = new Thread(this::test, "T2 ");
    thread1.start();
    thread2.start();
    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(A);
  }

  private void test() {
    lock.lock();
    for (int i = 0; i < 10000000; i++) {
      A++;
    }
    lock.unlock();
  }

}
